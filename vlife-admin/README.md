# vlife-admin
基于vlife低代码核心开发能力打造的开箱即用的权限管理应用，可在此系统之上进行二次开发

## 项目运行注意事项
1. java环境，使用jdk8
2. nodejs 16+
3. 启动项目之前需要运行 maven install命令（queryDsl生成相关文件）
