package cn.wwwlike.oa.dao;

import cn.wwwlike.oa.entity.ProjectTask;
import cn.wwwlike.vlife.core.dsl.DslDao;
import org.springframework.stereotype.Repository;

@Repository
public class ProjectTaskDao extends DslDao<ProjectTask> {
}
