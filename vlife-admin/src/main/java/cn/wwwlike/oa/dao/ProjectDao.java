package cn.wwwlike.oa.dao;

import cn.wwwlike.oa.entity.Project;
import cn.wwwlike.vlife.bi.BIDao;
import cn.wwwlike.vlife.core.dsl.DslDao;
import org.springframework.stereotype.Repository;

@Repository
public class ProjectDao extends BIDao<Project> {

}
